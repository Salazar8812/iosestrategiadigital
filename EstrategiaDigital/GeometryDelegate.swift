//
//  GeometryDelegate.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit


public protocol GeometryDelegate: NSObjectProtocol {
    
    func onSendingGeometry()
    
    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent])
    
    func onErrorGeometry(errorMessage : String)
    
    func onErrorConnectionGeometry()
    
}

