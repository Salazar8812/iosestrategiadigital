//
//  GeometryPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class GeometryPresenter : BasePresenter {
    
    var mGeometryDelegate : GeometryDelegate?
    var request : Alamofire.Request?
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/details/json"
    
    init(delegate : GeometryDelegate) {
        super.init()
        self.mGeometryDelegate = delegate
    }
    
    func getLatLon(placeId : String, googleMapKey: String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mGeometryDelegate?.onSendingGeometry()
            
            let urlString = "\(baseURLString)?placeid=\(placeId)&key=\(googleMapKey)"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<GeometryResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : GeometryResponse = response.result.value!
                    if((objectReponse.result?.geometry?.location?.lat != nil)){
                        self.mGeometryDelegate?.onSuccessGeometry(geoLatLon: (objectReponse.result?.geometry!)!, formatAddress: (objectReponse.result?.formattedAddress)!)
                    }else{
                        self.mGeometryDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                    self.mGeometryDelegate?.onErrorGeometry(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.mGeometryDelegate?.onErrorConnectionGeometry()
        }
    }
    
}

