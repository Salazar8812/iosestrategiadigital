//
//  InfoDataSource.swift
//  Afore
//
//  Created by Charls Salazar on 05/05/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import Foundation
import UIKit

public protocol DirectionTableViewDelegate : NSObjectProtocol {
    func onItemClick(addressSelection: PredictionAddress)
}

class DirectionAutocompleteDataSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    var tableView: UITableView?
    var predictionArray : [PredictionAddress] = []
    var directionTableViewDelegate : DirectionTableViewDelegate?
    
    init(tableView:UITableView, tableViewDelegate: DirectionTableViewDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 65
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.directionTableViewDelegate = tableViewDelegate
    }
    
    func update(coleccionInfo: [PredictionAddress]){
        self.predictionArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.predictionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CellDirectionTableViewCell"
       
        let cell:CellDirectionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! CellDirectionTableViewCell
        
        let address = self.predictionArray[indexPath.row]
    
        cell.titleLocationLabel.text = address.terms[0].value
        cell.directionLabel.text = address.description_
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let address = self.predictionArray[indexPath.row]
        self.directionTableViewDelegate?.onItemClick(addressSelection: address)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        selectedCell.contentView.backgroundColor = UIColor.red
    }
    
    // if tableView is set in attribute inspector with selection to multiple Selection it should work.
    
    // Just set it back in deselect
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        var cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
        //cellToDeSelect.contentView.backgroundColor = UIColor(netHex: 0x5D758F)
    }

    
}
