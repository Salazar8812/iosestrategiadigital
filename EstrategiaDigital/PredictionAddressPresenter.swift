//
//  PredictionAddressPresenter.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 27/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class PredictionAddressPresenter : BasePresenter {
    
    var mPredictionDelegate : PredictionAddressDelegate?
    var request : Alamofire.Request?
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    
    init(delegate : PredictionAddressDelegate) {
        super.init()
        self.mPredictionDelegate = delegate
    }
    
    func getPredictionAddress(googleMapsKey : String, keyword: String){
        if ConnectionUtils.isConnectedToNetwork(){
            self.mPredictionDelegate?.onSendingPrediction()
            
            let urlString = "\(baseURLString)?key=\(googleMapsKey)&input=\(keyword)"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<DirectionResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : DirectionResponse = response.result.value!
                    if(!objectReponse.predictionData.isEmpty){
                        self.mPredictionDelegate?.onSuccessPrediction(collectionAddress: objectReponse.predictionData)
                    }else{
                        self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                    self.mPredictionDelegate?.onErrorPrediction(errorMessage: "Ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.mPredictionDelegate?.onErrorConnection()
        }
    }
    
}
