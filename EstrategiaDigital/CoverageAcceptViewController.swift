//
//  CoverageAcceptViewController.swift
//  EstrategiaDigital
//
//  Created by Charls Salazar on 28/06/17.
//  Copyright © 2017 Charls Salazar. All rights reserved.
//

import UIKit

class CoverageAcceptViewController: UIViewController {
   
    override func viewDidLoad() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    
}
