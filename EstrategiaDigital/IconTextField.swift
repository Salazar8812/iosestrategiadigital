//
//  IconTextField.swift
//  VentasTotalPlayiOS
//
//  Created by Daniel García Aldynamic varado on 12/16/16.
//  Copyright © 2016 TotalPlay. All rights reserved.
//

import UIKit
import Material
import ActionSheetPicker_3_0

@IBDesignable class IconTextField: ErrorTextField, UITextFieldDelegate {
    
    @IBInspectable var iconLeft : UIImage? = nil
    @IBInspectable var maxCharacters : Int = 50
    var elements : [String]? = nil
    var indexElementPicker : Int = 0
    var isEnablePicker : Bool = true
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
    }
    
    override func draw(_ rect: CGRect) {
        if iconLeft != nil{
            self.leftView = UIImageView(image: iconLeft)
            self.leftView?.contentMode = UIViewContentMode.scaleAspectFit
            self.leftViewMode = .always
        } else {
            self.leftView = nil
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= maxCharacters // Bool
    }
    
    
   /* func setDatePicker(){
        self.removeTarget(self, action: nil, for: UIControlEvents.allEvents)
        self.addTarget(self, action:#selector(tapDateBlurButton(_:)), for: UIControlEvents.editingDidBegin)
    }
    
    func setTextPicker(elements : [String]){
        if self.elements == nil{
            self.removeTarget(self, action: nil, for: UIControlEvents.allEvents)
            self.addTarget(self, action:#selector(tapBlurButton(_:)), for: UIControlEvents.editingDidBegin)
        }
        self.elements = elements
        //_ = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
    }
    
    func tapBlurButton(_ sender: UITapGestureRecognizer) {
        showActionPicker()
    }
    
    func tapDateBlurButton(_ sender: UITapGestureRecognizer) {
        showActionDatePicker(date: self.text)
    }
    
    func showActionDatePicker(date : String? = nil){
        var showDate : Date? = nil
        if date != nil && date != "" {
            showDate = DateUtils.toDate(date: date!)
        } else {
            let today = Date()
            showDate = Calendar.current.date(byAdding: .year, value: -19, to: today)!
        }
        self.resignFirstResponder()
        let datePicker = ActionSheetDatePicker(title: "Fecha:", datePickerMode: UIDatePickerMode.date, selectedDate: showDate, doneBlock: {
            picker, value, index in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = DateUtils.toString(date: value as! Date)
            
            _ = self.becomeFirstResponder()
            self.text = "\(dateString)"
            self.resignFirstResponder()
            return
        }, cancel: { ActionStringCancelBlock in
            _ = self.becomeFirstResponder()
            self.resignFirstResponder()
            return
        }, origin: self)
        
        datePicker?.show()
    }
    
    func showActionPicker(){
        if isEnablePicker {
            self.resignFirstResponder()
            ActionSheetStringPicker.show(withTitle: self.placeholderLabel.text, rows: elements, initialSelection: 0, doneBlock: {
                picker, value, index in
                _ = self.becomeFirstResponder()
                self.indexElementPicker = value
                self.text = index as! String?
                self.resignFirstResponder()
                return
            }, cancel: { ActionStringCancelBlock in
                _ = self.becomeFirstResponder()
                self.resignFirstResponder()
                return
            }, origin: self)
        }
    }*/
    
    
}
